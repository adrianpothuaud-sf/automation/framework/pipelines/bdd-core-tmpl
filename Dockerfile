FROM registry.gitlab.com/adrianpothuaud-sf/automation/framework/bdd-core:latest

WORKDIR /usr/app

ADD features /usr/app/src/test/resources/features
ADD data /usr/app/src/test/resources/data
