#!/bin/bash

########################################
# This script is part of the build phase of CI pipeline
########################################

echo ""
echo "Start Script .bash/master/build/docker_push !"
echo ""
echo "*******************************"
echo ""
echo "         DOCKER_PUSH"
echo ""
echo "*******************************"
echo ""
echo "Pushes the Docker image to registry ..."
echo "-------------------------------"
echo ""

########################################
# here we push the docker image to Gitlab Registry
########################################
docker push $CI_REGISTRY_IMAGE:latest

echo ""
echo "End of Script !"
echo ""