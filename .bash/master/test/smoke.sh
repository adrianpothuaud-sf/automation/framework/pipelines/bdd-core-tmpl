#!/bin/bash

########################################
# This script is part of the test phase of CI pipeline
########################################

echo ""
echo "Start Script .bash/test/smoke.sh !"
echo ""
echo "*******************************"
echo ""
echo "       DOCKER_MVN_TEST"
echo ""
echo "*******************************"
echo ""
echo "Test the Docker image localy ..."
echo "-------------------------------"
echo ""

########################################
# ...
########################################
docker run $CI_REGISTRY_IMAGE:latest mvn test -Dcucumber.options="--tags ~@Critical"

echo ""
echo "End of Script !"
echo ""