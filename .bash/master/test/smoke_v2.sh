#!/bin/bash

########################################
# This script is part of the test phase of CI pipeline
########################################

echo ""
echo "Start Script .bash/test/smoke.sh !"
echo ""
echo "*******************************"
echo ""
echo "           MVN_TEST"
echo ""
echo "*******************************"
echo ""
echo "Run the tests ..."
echo "-------------------------------"
echo ""

########################################
# ...
########################################
cd /usr/app || return
mvn -q clean install -Dcucumber.options="--tags ~@Critical"

echo ""
echo "End of Script !"
echo ""