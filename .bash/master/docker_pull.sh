#!/bin/bash

########################################
# This script is part of the deploy phase of CI pipeline
########################################

echo ""
echo "Start Script .bash/master/deploy/docker_pull !"
echo ""
echo "*******************************"
echo ""
echo "         DOCKER_PULL"
echo ""
echo "*******************************"
echo ""
echo "Pull the Docker image from registry ..."
echo "-------------------------------"
echo ""

########################################
# here we push the docker image to Gitlab Registry
########################################
docker pull $CI_REGISTRY_IMAGE:latest

echo ""
echo "End of Script !"
echo ""