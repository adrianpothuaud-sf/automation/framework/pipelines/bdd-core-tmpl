#!/bin/bash

########################################
# This script is part
# of CI pipeline for issue branches
########################################

echo ""
echo "Start Script .bash/issue/docker_pull !"
echo ""
echo "*******************************"
echo ""
echo "         DOCKER_PULL"
echo ""
echo "*******************************"
echo ""
echo "Pull the Docker image from registry ..."
echo "-------------------------------"
echo ""

########################################
# here we pull the docker image from Gitlab Registry
########################################
docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME-latest

echo ""
echo "End of Script !"
echo ""