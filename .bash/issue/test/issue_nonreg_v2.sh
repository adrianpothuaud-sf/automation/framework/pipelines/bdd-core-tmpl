#!/bin/bash

########################################
# This script is part of the test phase of CI pipeline
########################################

echo ""
echo "Start Script .bash/issue/test/issue_nonreg_v2.sh !"
echo ""
echo "*******************************"
echo ""
echo "          MVN_TEST"
echo ""
echo "*******************************"
echo ""
echo "Run the tests using Maven ..."
echo "-------------------------------"
echo ""

########################################
# ...
########################################
cd /usr/app || return
mvn -q clean install -Dcucumber.options="--tags @$CI_COMMIT_REF_NAME"

echo ""
echo "End of Script !"
echo ""