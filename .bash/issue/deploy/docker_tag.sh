#!/bin/bash

########################################
# This script is part of the deploy phase of CI pipeline
########################################

echo ""
echo "Start Script .bash/issue/deploy/docker_tag !"
echo ""
echo "*******************************"
echo ""
echo "         DOCKER_TAG"
echo ""
echo "*******************************"
echo ""
echo "Tags the Docker image localy ..."
echo "-------------------------------"
echo ""

########################################
# here we tag the docker image with stable
########################################
docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME-latest $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME-stable

echo ""
echo "End of Script !"
echo ""