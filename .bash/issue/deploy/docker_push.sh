#!/bin/bash

########################################
# This script is part of the deploy phase of CI pipeline
########################################

echo ""
echo "Start Script .bash/issue/deploy/docker_push !"
echo ""
echo "*******************************"
echo ""
echo "         DOCKER_PUSH"
echo ""
echo "*******************************"
echo ""
echo "Push the Docker image to registry ..."
echo "-------------------------------"
echo ""

########################################
# here we push the docker image to Gitlab Registry
########################################
docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME-stable

echo ""
echo "End of Script !"
echo ""