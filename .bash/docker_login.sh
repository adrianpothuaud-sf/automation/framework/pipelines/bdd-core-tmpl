#!/bin/bash

########################################
# This script is part of several phases of CI pipeline
########################################

echo ""
echo "Start Script .bash/docker_login !"
echo ""
echo "*******************************"
echo ""
echo "         DOCKER_LOGIN"
echo ""
echo "*******************************"
echo ""
echo "Login to Docker Hub ..."
echo "-------------------------------"
echo ""

########################################
# here we log in to docker Hub by the command line
# These variables are stored in CI
########################################
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

echo ""
echo "End of Script !"
echo ""