#!/bin/bash

########################################
# This script is part of the deploy phase of CI pipeline
########################################

echo ""
echo "Start Script .bash/deploy/docker_push !"
echo ""
echo "*******************************"
echo ""
echo "         DOCKER_PUSH"
echo ""
echo "*******************************"
echo ""
echo "Pull the Docker image from registry ..."
echo "-------------------------------"
echo ""

########################################
# here we push the docker image to Gitlab Registry
########################################
docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME-latest

echo ""
echo "End of Script !"
echo ""