#!/bin/bash

########################################
# This script is part of the test phase of CI pipeline
########################################

echo ""
echo "Start Script .bash/feature/test/ft_accept_v2.sh !"
echo ""
echo "*******************************"
echo ""
echo "           MVN_TEST"
echo ""
echo "*******************************"
echo ""
echo "Run the tests using Maven ..."
echo "-------------------------------"
echo ""

########################################
# Use branch name tag to run bdd tests
########################################
cd /usr/app || return
mvn -q clean install -Dcucumber.options="--tags @$CI_COMMIT_REF_NAME"

echo ""
echo "End of Script !"
echo ""