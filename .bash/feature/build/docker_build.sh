#!/bin/bash

########################################
# This script is part of the build phase
# of CI pipeline for feature branches
########################################

echo ""
echo "Start Script .bash/feature/build/docker_build !"
echo ""
echo "*******************************"
echo ""
echo "         DOCKER_BUILD"
echo ""
echo "*******************************"
echo ""
echo "Builds the Docker image localy ..."
echo "-------------------------------"
echo ""

########################################
# here we build the docker image localy
########################################
docker build -t $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME-latest .

echo ""
echo "End of Script !"
echo ""